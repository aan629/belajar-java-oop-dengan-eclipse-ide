//Class Child Person
public class Programmer extends Person {
	String technology;
	
	public Programmer() {
		//TODO Auto-Generated constructor stub
		//Sebenernya dalam ini ada statement 'super();' , cuman karena java compiler sudah otomatis ada.
	}
	
	public Programmer(String name, String address, String technology) {
		super(name, address); //Memanggil constructor dari parent
		this.technology = technology;
	}
	
	void coding() {
		System.out.println("I can create a application using technology : " + technology + ".");
	}
	
	void hacking() {
		System.out.println("I can hacking a website");
	}
	
	/*
	//Method Overriding dari method 'void greeting()' di parent class Person.java
	void greeting(){
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
		System.out.println("My Occupation is a " + technology + " programmer.");
	}
	*/
	
	//Nah dari pada lama ketik metod ulang dengan Method Overriding, kita pakai keywoard Super
		void greeting(){
			super.greeting(); //Memanggil method greeting dari parent atau person class (keywoard 'super' harus berada di barisan teratas statement)
			System.out.println("My Occupation is a " + technology + " programmer.");
		}
}
