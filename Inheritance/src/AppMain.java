//Object
public class AppMain {
	public static void main(String[] args) {
		//Impelementasi Object Person menggunakan constructor default yang lumayan capek ngodingnya ;)
		Person person1 = new Person();
		person1.name = "Hendra"; //field name dari class parent
		person1.address = "Garut"; //field address dari class parent
		
		//Impelementasi Object Teacher menggunakan constructor berparameter
		Teacher teacher1 = new Teacher("Budi", "Bandung", "Matematika");
		
		//Impelementasi Object Doctor menggunakan constructor default yang lumayan capek ngodingnya ;)
		Doctor doctor1 = new Doctor();
		doctor1.name = "Elis"; //field name dari class parent
		doctor1.address = "Jakarta"; //field address dari class parent
		doctor1.specialist = "Dentis";
		
		//Impelementasi Object Programmer menggunakan constructor berparameter
		Programmer programmer1 = new Programmer("Rizki", "Surabaya", "Javascript");
		
		//Memanggil method greeting() dari person
		person1.greeting();
		teacher1.greeting();
		doctor1.greeting();
		programmer1.greeting();
	}
}
