//Class Parent Person
public class Person {
	String name;
	String address;
	
	//Contructor baris 6-10 dilakukan dengan klik kanan mouse - lalu klik 'Source' - lalu klik 'Generate Constructor Using Field'
	public Person(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	public Person() {
		super();
	}
	
	void greeting() {
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
		
	}
}
