//Class Child Person
public class Doctor extends Person {
	String specialist;
	
	public Doctor() {
		//TODO Auto-Generated constructor stub
		//Sebenernya dalam ini ada statement 'super();' , cuman karena java compiler sudah otomatis ada.
	}
	
	public Doctor(String name, String address, String specialist) {
		super(name, address); //Memanggil constructor dari parent
		this.specialist = specialist;
	}
	
	void surgery() {
		System.out.println("I can surgery operation patients");
	}
	
	/*
	//Method Overriding dari method 'void greeting()' di parent class Person.java
	void greeting() {
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
		System.out.println("My Occupation is a " + specialist + " doctor.");
	}
	*/
	
	//Nah dari pada lama ketik metod ulang dengan Method Overriding, kita pakai keywoard Super
	void greeting() {
		super.greeting(); //Memanggil method greeting dari parent atau person class (keywoard 'super' harus berada di barisan teratas statement)
		System.out.println("My Occupation is a " + specialist + " doctor.");
	}
}
