//Class Child Person
public class Teacher extends Person {
	String subject;
	
	public Teacher() {
		//TODO Auto-Generated Constructor Stub
		//Sebenernya dalam ini ada statement 'super();' , cuman karena java compiler sudah otomatis ada.
	}
	
	public Teacher(String name, String address, String subject) {
		super(name, address); //Memanggil constructor dari parent
		/*Sebenernya 'super(name, address);', bisa dengan cara dibawah ini hanya saja tidak efektif\
		 * super();
		 * super.name = name;
		 * super.address = address
		 */
		this.subject = subject;
	}
	
	void teaching() {
		System.out.println("I can teach " + subject + ", So anyone how wants to learn can talk to me.");
	}
	
	/*
	//Method Overriding dari method 'void greeting()' di parent class Person.java
	void greeting() {
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
		System.out.println("My job is a " + subject + " teacher.");
	)
	*/
	
	//Nah dari pada lama ketik metod ulang dengan Method Overriding, kita pakai keywoard Super
	void greeting() {
		super.greeting(); //Memanggil method greeting dari parent atau person class(keywoard 'super' harus berada di barisan teratas statement)
		System.out.println("My job is a " + subject + " teacher.");
	}
}
