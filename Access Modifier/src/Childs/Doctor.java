package Childs;
import Parents.Person;

//Class Child Person
public class Doctor extends Person {
	public String specialist; //Tambah Access modifier 'public'
	
	public Doctor() {
		//TODO Auto-Generated constructor stub
	}
	
	public Doctor(String name, String address, String specialist) {
		super(name, address); 
		this.specialist = specialist;
	}
	
	public void surgery() { //Tambah Access modifier 'public'
		System.out.println("I can surgery operation patients");
	}
	
	public void greeting() { //Tambah Access modifier 'public'
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + "."); 
		System.out.println("My Occupation is a " + specialist + " doctor.");
	}
}
