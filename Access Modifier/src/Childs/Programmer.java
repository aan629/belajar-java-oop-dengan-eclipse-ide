package Childs;
import Parents.Person;

public class Programmer extends Person {
	public String technology; //Tambah Access modifier 'public'
	
	public Programmer() {
		//TODO Auto-Generated constructor stub
	}
	
	public Programmer(String name, String address, String technology) {
		super(name, address); 
		this.technology = technology;
	}
	
	public void coding() { //Tambah Access modifier 'public'
		System.out.println("I can create a application using technology : " + technology + ".");
	}
	
	public void hacking() { //Tambah Access modifier 'public'
		System.out.println("I can hacking a website");
	}
	
	@Override
	public void greeting(){
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
		System.out.println("My Occupation is a " + technology + " programmer.");
	}
}
