package Childs;
import Parents.Person;

//Class Child Person
public class Teacher extends Person {
	public String subject; //Tambah Access modifier 'public'
	
	public Teacher() {
		//TODO Auto-Generated constructor stub
	}
	
	public Teacher(String name, String address, String subject) {
		super(name, address); 
		this.subject = subject;
	}
	
	public void teaching() { //Tambah Access modifier 'public'
		System.out.println("I can teach " + subject + ", So anyone how wants to learn can talk to me.");
	}
	
	public void greeting() {
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + "."); 
		System.out.println("My job is a " + subject + " teacher.");
	}
}
