package Parents;
public class Person {
	public String name; //Tambah Access modifier 'public'
	public String address; //Tambah Access modifier 'public'
	
	public Person(String name, String address) { //Constructor
		super();
		this.name = name;
		this.address = address;
	}

	public Person() { //Constructor
		super();
	}
	
	public void greeting() { //Method Tambah Access modifier 'public'
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
	}
}
