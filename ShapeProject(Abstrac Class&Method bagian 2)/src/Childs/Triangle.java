package Childs;

import Parents.Shape;

public class Triangle extends Shape {
	private double base;
	private double height;
	
	//Baris 10-12 Ketik keywoard 'Triangle', lalu klik ctrl+space, lalu pilih 'Triangle() - Constructor'
	public Triangle() {
		// TODO Auto-generated constructor stub
	}
	
	public Triangle(double base, double height, String color) {
		setColor(color);
		this.base = base;
		this.height = height;
	}
	
	//Baris 22-26 otomatis dilakukan dengan cara ketik keywoard 'get', lalu ketik ctrl+space, lalu pilih 'getArea(): double - Override method in 'Shape' '
	//Override dan implementasi method abstract getArea
	@Override
	public double getArea() {
		double area = 0.5 * base * height; // Baris 24 dibuat manual semua
		return area; //Baris 25 dibuat manual dengan mengubah angka 0 menjadi 'area'
	}
}
