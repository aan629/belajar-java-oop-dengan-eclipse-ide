package Childs;

import Parents.Shape;

public class Circle extends Shape {
	private double radius;
	
	//Baris 9-11 Ketik keywoard 'Circle', lalu klik ctrl+space, lalu pilih 'Circle() - Constructor'
	public Circle() {
		// TODO Auto-generated constructor stub
	}
	
	public Circle (String color, double radius) {
		setColor(color);
		this.radius = radius;
	}
	
	//Buat Getter and Setter dengan otomatis dengan cara klik kanan mouse, lalu klik 'source', lalu klik 'Generate Getters and Setters'
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	//Baris 29-33 otomatis dilakukan dengan cara ketik keywoard 'get', lalu ketik ctrl+space, lalu pilih 'getArea(): double - Override method in 'Shape' '
	//Override dan implementasi method abstract getArea untuk class Circle
	@Override
	public double getArea() {
		double area = Math.PI * radius * radius; // Baris 31 dibuat manual semua
		return area; //Baris 25 dibuat manual dengan mengubah angka 0 menjadi 'area'
	}
}
