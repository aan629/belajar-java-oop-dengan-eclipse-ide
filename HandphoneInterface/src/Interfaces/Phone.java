package Interfaces;

public interface Phone { //keyword 'class' diganti 'interface'
	//Constant field/attribute, sehingga memakai huruf besar. Di interface field/attribut harus sudah final(Tetap absolut gk bisa diubah)
	int MAX_VOLUME = 100; //Tidak pakai keywoard 'final', karena di interface sudah otomatis
	int MIN_VOLUME = 0; //Tidak pakai keywoard 'final', karena di interface sudah otomatis
	
	//Abstract method
	void powerOn();
	void powerOff();
	void volumeUp();
	void volumeDown();
}