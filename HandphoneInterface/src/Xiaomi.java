import Interfaces.Phone;

public class Xiaomi implements Phone{
	private static final int MIN_VOLUME = 0;
	private static final int MAX_VOLUME = 0;
	private int volume;
	private boolean isPowerOn;
	
	//Baris 7-9 dibuat otomatis dengan cara ketik keywoard 'Xiaomi', lalu klik ctrl+space, lalu pilih 'Xiaomi() - Constructor'
	public Xiaomi() {
		// Set volume awal default
		this.volume = 50;
	}
	
	//Override semua methode abstract yang ada di interface Phone
	@Override
	public void powerOn() {
		isPowerOn = true;
		System.out.println("Handphone menyala .......");
		System.out.println("Selamat datang di Xiaomi ");
		System.out.println("Android version 10");
	}
	
	@Override
	public void powerOff() {
		isPowerOn = false;
		System.out.println("Mematikan handphone ");
	}
	
	@Override
	public void volumeUp() {
		if(isPowerOn) {
			if(this.volume == MAX_VOLUME) {
				System.out.println("Volume sudah Maksimal!!!");
				System.out.println("Volume = " + this.volume + "%");
			}
			else {
				this.volume += 10;
				System.out.println("Volume sekarang " + this.volume);
			}
		}
		else {
			System.out.println("Handphone mati, silahkan nyalakan dulu!!!");
		}
	}
	
	public void volumeDown() {
		if(isPowerOn) {
			if(this.volume == MIN_VOLUME) {
				System.out.println("Volume = " + this.volume + "%");
			}
			else {
				this.volume -= 10;
				System.out.println("Volume sekarang " + this.volume);
			}
		}
		else {
			System.out.println("Handphone mati, silahkan nyalakan dulu!!!");
		}
	}

	//Getter dan Setter
	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public boolean isPowerOn() {
		return isPowerOn;
	}

	public void setPowerOn(boolean isPowerOn) {
		this.isPowerOn = isPowerOn;
	}
	
	
}
