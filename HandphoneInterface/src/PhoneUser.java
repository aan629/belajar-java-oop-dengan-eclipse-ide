import Interfaces.Phone; //Otomatis dihasilkan ketika baris 4 mengetik 'private Phone', lalu klik ctrl+space, lalu pilih 'Phone - interfaces'

public class PhoneUser {
	private Phone phone;
	
	//Baris 7-9 dibuat otomatis dengan cara ketik keywoard (Phone), lalu klik ctrl+space, lalu pilih 'PhoneUser() - Constructor'
	public PhoneUser(Phone phone) { // 'Phone phone' dalam tanda kurung dibuat manual
		this.phone = phone; //Baris ini dibuat manual semua
	}
	
	public void turnOnThePhone() {
		this.phone.powerOn();
	}
	
	public void turnOffThePhone() {
		this.phone.powerOff();
	}
	
	public void makePhoneLouder() {
		this.phone.volumeUp();
	}
	
	public void makePhoneSilent() {
		this.phone.volumeDown();
	}

	//Getter and Setter karena di baris ke 4 ada access modifier private
	public Phone getPhone() {
		return this.phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}
	
	
	
}
