package Childs;
import Parents.Person;

public class Programmer extends Person{
	private String technology; //Access modifier Encapsulation(private) pada semua field/attribute class child
	
	public Programmer() {
		//TODO Auto-Generated constructor stub
	}
	
	public Programmer(String name, String address, String technology) {
		super(name, address); 
		this.technology = technology;
	}
	
	public void coding() { 
		System.out.println("I can create a application using technology : " + technology + ".");
	}
	
	public void hacking() {
		System.out.println("I can hacking a website");
	}
	
	@Override
	public void greeting(){
		System.out.println("Hello my name is " + getName() + "."); //Menyesuaikan getter, dari sebelumnya 'name' ke 'getName()'
		System.out.println("I come from " + getAddress() + "."); //Menyesuaikan getter, dari sebelumnya 'address' ke 'getAddress()
		System.out.println("My Occupation is a " + technology + " programmer.");
	}
	
	//Baris 32-38 dibuat otomatis dengan cara klik kanan mouse, lalu klik 'source', lalu klik generate getter and setter
	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}
}
