package Childs;
import Parents.Person;

public class Doctor extends Person{
	private String specialist; //Access modifier Encapsulation(private) pada semua field/attribute class child
	
	public Doctor() {
		//TODO Auto-Generated constructor stub
	}
	
	public Doctor(String name, String address, String specialist) {
		super(name, address); 
		this.specialist = specialist;
	}
	
	public void surgery() { 
		System.out.println("I can surgery operation patients");
	}
	
	public void greeting() { 
		System.out.println("Hello my name is " + getName() + "."); //Menyesuaikan getter, dari sebelumnya 'name' ke 'getName()'
		System.out.println("I come from " + getAddress() + "."); //Menyesuaikan getter, dari sebelumnya 'address' ke 'getAddress()
		System.out.println("My Occupation is a " + specialist + " doctor.");
	}
	
	//Setter dan Getter
	//Baris 27-34 dibuat otomatis dengan cara klik kanan mouse, lalu klik 'source', lalu klik generate getter and setter
	public String getSpecialist() {
		return this.specialist;
	}
	
	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}
}
