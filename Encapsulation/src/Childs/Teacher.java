package Childs;
import Parents.Person;

public class Teacher extends Person {
	private String subject; //Access modifier Encapsulation(private) pada semua field/attribute class child
	
	public Teacher() {
		//TODO Auto-Generated constructor stub
	}
	
	public Teacher(String name, String address, String subject) {
		super(name, address); 
		this.subject = subject;
	}
	
	public void teaching() { 
		System.out.println("I can teach " + subject + ", So anyone how wants to learn can talk to me.");
	}
	
	public void greeting() {
		System.out.println("Hello my name is " + getName() + "."); //Menyesuaikan getter, dari sebelumnya 'name' ke 'getName()'
		System.out.println("I come from " + getAddress() + "."); //Menyesuaikan getter, dari sebelumnya 'address' ke 'getAddress()
		System.out.println("My job is a " + subject + " teacher.");
	}
	
	//Setter dan Getter
	//Baris 27-33 dibuat otomatis dengan cara klik kanan mouse, lalu klik 'source', lalu klik generate getter and setter
	public String getSubject() {
		return this.subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
}
