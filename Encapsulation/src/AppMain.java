import Childs.Doctor;
import Childs.Programmer;
import Childs.Teacher;
import Parents.Person;

public class AppMain {
	public static void main(String[] args) {
		Person person1 = new Programmer("Rizky", "Bandung", ".Net Core"); //Menyimpan variabel 'Rizky', "Bandung", ".Net Core" ke variabel 'person1' bertipe data Person. Hal ini bisa dilakukan karena class 'Programmer' termasuk dari class 'Person' juga
		Person person2 = new Teacher("Joko", "Tegal", "Matematika");
		Person person3 = new Doctor("Eko", "Surabaya", "Pedistrician");
		
		sayHello(person1);
		sayHello(person2);
		sayHello(person3);
	}
	
	static void sayHello(Person person) {
		String message;
		if(person instanceof Programmer) {
			Programmer programmer = (Programmer) person;
			message = "Hello, " + programmer.getName() + ". Seorang Programmer " + programmer.getTechnology() + "."; //Menyesuaikan getter, dari sebelumnya 'name' ke 'getName()' dan dari sebelumnya 'address' ke 'getAddress()
		}
		else if(person instanceof Teacher) {
			Teacher teacher = (Teacher) person;
			message = "Hello, " + teacher.getName() + ". Seorang Guru " + teacher.getSubject() + "."; //Menyesuaikan getter, dari sebelumnya 'name' ke 'getName()' dan dari sebelumnya 'address' ke 'getAddress()
		}
		else if(person instanceof Doctor) {
			Doctor doctor = (Doctor) person;
			message = "Hello, " + doctor.getName() + ". Seorang Dokter " + doctor.getSpecialist() + "."; //Menyesuaikan getter, dari sebelumnya 'name' ke 'getName()' dan dari sebelumnya 'address' ke 'getAddress()
		}
		else {
			message = "Hello, " + person.getName() + ".";
		}
		System.out.println(message);
	}
}
