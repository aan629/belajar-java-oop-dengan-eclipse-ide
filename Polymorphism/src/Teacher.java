//Class Child Person
//Ada 2 buah constructor dan 2 Method
public class Teacher extends Person {
	String subject;
	
	public Teacher() {
		//TODO Auto-Generated constructor stub
	}
	
	public Teacher(String name, String address, String subject) {
		super(name, address); 
		this.subject = subject;
	}
	
	void teaching() {
		System.out.println("I can teach " + subject + ", So anyone how wants to learn can talk to me.");
	}
	
	void greeting() {
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + "."); 
		System.out.println("My job is a " + subject + " teacher.");
	}
}
