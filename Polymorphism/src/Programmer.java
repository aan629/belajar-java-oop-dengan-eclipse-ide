//Class Child Person
//Ada 2 buah constructor dan 3 Method yang dimana salah satunya sudah di override
public class Programmer extends Person{
String technology;
	
	public Programmer() {
		//TODO Auto-Generated constructor stub
	}
	
	public Programmer(String name, String address, String technology) {
		super(name, address); 
		this.technology = technology;
	}
	
	void coding() {
		System.out.println("I can create a application using technology : " + technology + ".");
	}
	
	void hacking() {
		System.out.println("I can hacking a website");
	}
	
	@Override
	void greeting(){
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
		System.out.println("My Occupation is a " + technology + " programmer.");
	}
}
