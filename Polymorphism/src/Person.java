//Class Parent Person
//Ada 2 buah constructor dan 1 Method
public class Person {
	String name;
	String address;
	
	public Person(String name, String address) { //Constructor
		super();
		this.name = name;
		this.address = address;
	}

	public Person() { //Constructor
		super();
	}
	
	void greeting() { //Method
		System.out.println("Hello my name is " + name + ".");
		System.out.println("I come from " + address + ".");
	}
}
