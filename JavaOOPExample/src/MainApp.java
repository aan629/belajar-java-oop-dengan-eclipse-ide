
public class MainApp {

	public static void main(String[] args) {
		//Membuat object dari class Person menggunakan Constructor 2 Parameter
		Person person1 = new Person("Eko", "Tegal"); // 2 Parameter
		person1.sayHello("Padepokan 79");
		System.out.println(person1.sayAddress());
		
		//Membuat object dari class Person menggunakan Constructor Default
		Person person2 = new Person(); // Default
		person2.name = "Joko";
		person2.address = "Surabaya";
		person2.sayHello("Padepokan 79");
		System.out.println(person2.sayAddress());
		
		//Membuat object dari class Person menggunakan Constructor satu parameter
		Person person3 = new Person("Budi");  // 1 Parameter
		person3.address = "Bandung";
		person3.sayHello("Padepokan 79");
		System.out.println(person3.sayAddress());
		
		/*
		 * Perumpamaan Kondisi Pembuatan Object pada Atrribut/Field dan method saja hanya dengan kodingan di bawah ini
		 * 
		Person person1 = new Person(); //Membuat Object
		person1.name = "Joko"; //Instansiasi object (field)
		person1.address = "Surabaya"; //Instansiasi object (field)
		
		person1.sayHello("Padepokan 79");
		System.out.println(person1.sayAddress());
		*/
	}
}
