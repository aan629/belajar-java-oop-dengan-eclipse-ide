
public class Person {
	//3 String name dibawah adalah field/Atribute
	String name; 
	String address;
	final String country = "Indonesia";
	
	//Constructor Default = Constructor yang biasanya disediakan sama java compiler ketika kita tidak menuliskan atau mendeklarasikan sebuah constructor dalam class 
	Person(){
		
	}
	
	//Constructor dengan 1 Parameter
	Person(String paramName){
		name = paramName;
	}
	
	//Constructor dengan 2 Parameter
	Person(String paramName, String paramAddress){
		this(paramName); //Pemanggilan sebuah constructor overloading harus berada pada baris pertama body statement, jika tidak maka akan terjadi error
		address = paramAddress;
	}
	
	/* Misal jika Constructor dengan 2 Parameter Shadowing
	 * 
	 * Person(String name, String address){
	 *	//Penamaan nama parameter dan nama field yang sama, sehingga terjadi shadowing variable. Solusinya menggunakan keyboard this.
	 *  //Pemakaian this harus di baris pertama statement
	 * 	this.name = name; //this.name ini menunjukkan field name adalah cari class Person itu sendiri.
	 * 	this.address = address;	//this.address ini menunjukkan field address adalah dari class person itu sendiri.
	 * }
	 * */
	
	//Method void
	void sayHello(String paramName){
		System.out.println("Hello " + paramName + ", Myname is " + name + ".");
	}
	
	//Method return value (Mengembalikan nilai)
	String sayAddress() {
		return "I come from " + address + ".";
	}
}
