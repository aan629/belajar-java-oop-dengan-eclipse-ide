import Childs.Item; //Otomatis dengan cara ketika sudah mengetik di baris 6 yaitu 'Product product2 = new Item', langsung klik ctrl+space, lalu pilih Item() - childs.Item
import Parents.Product;

public class MainApp {
	Product product1 = new Product(); //Tidak dapat langsung diinstansiasi menjadi object karena class Product adalah abstract.
	Product product2 = new Item(); //Dapat membuat object Product dari instansiasi child class item.
}
